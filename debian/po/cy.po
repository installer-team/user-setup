# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of Debian Installer templates to Welsh
# Copyright (C) 2004-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Jonathan Price <mynamesnotclive@notclive.co.uk>, 2008.
#
# Translations from iso-codes:
#   Alastair McKinstry <mckinstry@debian.org>, 2004.
#   - translations from ICU-3.0
#   Dafydd Harries <daf@muse.19inch.net>, 2002,2004,2006.
#   Free Software Foundation, Inc., 2002,2004
#   Alastair McKinstry <mckinstry@computer.org>, 2001
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: user-setup@packages.debian.org\n"
"POT-Creation-Date: 2024-04-06 20:02+0000\n"
"PO-Revision-Date: 2023-10-16 04:19+0000\n"
"Last-Translator: Rhodri <rhodrilld@gmail.com>\n"
"Language-Team: Welsh <>\n"
"Language: cy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=(n==0) ? 0 : (n==1) ? 1 : (n==2) ? 2 : "
"(n==3) ? 3 :(n==6) ? 4 : 5;\n"

#. Type: boolean
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:5001
msgid "Allow login as root?"
msgstr "Galluogi mewngofnodi fel root?"

#. Type: boolean
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:5001
msgid ""
"If you choose not to allow root to log in, then a user account will be "
"created and given the power to become root using the 'sudo' command."
msgstr ""
"Os ydych yn dewis peidio gadael root i fewngofnodi, yna bydd cyfrif "
"defnyddiwr yn gael ei creu ac yn gael y pwêr i ddod yn root trwy defnyddio'r "
"gorchymyn 'sudo'."

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:6001
msgid "Root password:"
msgstr "Cyfrinair 'root':"

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:6001
msgid ""
"Some account needs to be available with administrative super-user "
"privileges. The password for that account should be something that cannot be "
"guessed."
msgstr ""

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:6001
msgid ""
"To allow direct password-based access via the 'root' account, you can set "
"the password for that account here."
msgstr ""

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:6001
msgid ""
"Alternatively, you can lock the root account's password by leaving this "
"setting empty, and instead use the system's initial user account (which will "
"be set up in the next step) to gain administrative privileges. This will be "
"enabled for you by adding that initial user to the 'sudo' group."
msgstr ""

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:6001
msgid "Note: what you type here will be hidden (unless you select to show it)."
msgstr ""

#. Type: password
#. Description
#. :sl1:
#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:7001 ../user-setup-udeb.templates:14001
msgid "Re-enter password to verify:"
msgstr "Ail-rhowch y cyfrinair er mwyn dilysu:"

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:7001
msgid ""
"Please enter the same root password again to verify that you have typed it "
"correctly."
msgstr ""
"Os gwelwch yn dda, rhowch yr un cyfrinair 'root' eto er mwyn gwirio eich bod "
"wedi ei deipio'n gywir."

#. Type: boolean
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:8001
msgid "Create a normal user account now?"
msgstr "Creu cyfrif defnyddiwr arferol nawr?"

#. Type: boolean
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:8001
msgid ""
"It's a bad idea to use the root account for normal day-to-day activities, "
"such as the reading of electronic mail, because even a small mistake can "
"result in disaster. You should create a normal user account to use for those "
"day-to-day tasks."
msgstr ""
"Mae'n syniad gwael defnyddio'r cyfrinair 'root' ar gyfer defnydd cyffredinol "
"megis darllen ebost, oherwydd gall hyd yn oed gamgymeriad bychan achosi "
"trychineb. Dylech greu cyfrif defnyddiwr cyffredin i'w ddefnyddio ar gyfer "
"tasgau cyffredinol."

#. Type: boolean
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:8001
#, fuzzy
msgid ""
"Note that you may create it later (as well as any additional account) by "
"typing 'adduser <username>' as root, where <username> is a username, like "
"'imurdock' or 'rms'."
msgstr ""
"Fe allwch chi ei greu nes ymlaen (ynghyd a unrhyw gyfrifon ychwanegol) drwy "
"deipio 'adduser <enw defnyddiwr>' fel root, lle mae <enw defnyddiwr> yn enw "
"cyfrif, fel 'imurdock' neu 'rms'."

#. Type: string
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:9001
msgid "Full name for the new user:"
msgstr "Enw llawn ar gyfer y defnyddiwr newydd:"

#. Type: string
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:9001
msgid ""
"A user account will be created for you to use instead of the root account "
"for non-administrative activities."
msgstr ""
"Caiff cyfrif newydd ei greu ar eich cyfer i'w ddefnyddio yn hytrach na'r "
"cyfrif 'root' ar gyfer gweithgareddau nad ydynt yn weinyddol."

#. Type: string
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:9001
msgid ""
"Please enter the real name of this user. This information will be used for "
"instance as default origin for emails sent by this user as well as any "
"program which displays or uses the user's real name. Your full name is a "
"reasonable choice."
msgstr ""
"Rhowch enw go iawn y defnyddiwr hwn. Caiff y gwybodaeth hwn ei ddefnyddio er "
"enghraifft fel tardd diofyn ar gyfer ebost mae'r defnyddiwr hwn yn ei "
"ddanfon ynghyd a unrhyw rhaglen sy'n dangos neu ddefnyddio enw go iawn y "
"defnyddiwr. Mae eich enw llawn yn ddewis rhesymol."

#. Type: string
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:10001
msgid "Username for your account:"
msgstr "Enw defnyddiwr ar gyfer eich cyfrif:"

#. Type: string
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:10001
msgid ""
"Select a username for the new account. Your first name is a reasonable "
"choice. The username should start with a lower-case letter, which can be "
"followed by any combination of numbers and more lower-case letters."
msgstr ""
"Dewiswch enw defnyddiwr ar gyfer y cyfrif newydd. Mae eich enw cyntaf yn "
"ddewis rhesymol. Dylai'r enw cyfrif gychwyn gyda llythyren fechan, wedi ei "
"ddilyn gan unrhyw gyfuniad o rhifau a mwy o lythrennau bychan."

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:11001
msgid "Invalid username"
msgstr "Enw defnyddiwr annilys"

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:11001
msgid ""
"The username you entered is invalid. Note that usernames must start with a "
"lower-case letter, which can be followed by any combination of numbers and "
"more lower-case letters, and must be no more than 32 characters long."
msgstr ""
"Mae'r enw defnyddiwr a roddwyd yn annilys. Mae'n rhaid i enw ddefnyddiwr "
"ddechrau gyda llythyren bach, wedi ei ddilyn gyda unrhyw gyfuniad o rifau a "
"mwy o lythrennau bach, ddim yn hirach na 32 llythyren."

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:12001
msgid "Reserved username"
msgstr "Enw defnyddiwr neilltuedig"

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:12001
msgid ""
"The username you entered (${USERNAME}) is reserved for use by the system. "
"Please select a different one."
msgstr ""
"Mae'r enw defnyddiwr a mewnosodwyd (${USERNAME}) wedi ei neillto ar gyfer "
"defnydd gan y system. Dewiswch un gwahanol."

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:13001
msgid "Choose a password for the new user:"
msgstr "Dewiswch gyfrinair ar gyfer y defnyddiwr newydd:"

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:13001
msgid "Make sure to select a strong password that cannot be guessed."
msgstr ""

#. Type: password
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:14001
msgid ""
"Please enter the same user password again to verify you have typed it "
"correctly."
msgstr ""
"Os gwelwch yn dda, rhowch yr un cyfrinair defnyddiwr eto er mwyn gwirio eich "
"bod chi wedi ei deipio'n gywir."

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:15001
msgid "Password input error"
msgstr "Gwall mewnbwn cyfrinair"

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:15001
msgid "The two passwords you entered were not the same. Please try again."
msgstr ""
"Nid oedd y ddau gyfrinair y rhoddoch yr yn peth. Ceisiwch eto os gwelwch yn "
"dda."

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:16001
msgid "Empty password"
msgstr "Cyfrinair gwag"

#. Type: error
#. Description
#. :sl2:
#: ../user-setup-udeb.templates:16001
msgid ""
"You entered an empty password, which is not allowed. Please choose a non-"
"empty password."
msgstr ""
"Fe rhoddoch gyfrinair gwag, ond ni chaniateir hyn. Dewiswch gyfrinair nad "
"yw'n wag."

#. Type: title
#. Description
#. :sl1:
#: ../user-setup-udeb.templates:17001
msgid "Set up users and passwords"
msgstr "Paratoi defnyddwyr a chyfrineiriau"

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../user-setup-udeb.templates:18001
msgid "Setting users and passwords..."
msgstr "Gosod defnyddwyr a chyfrineiriau..."
